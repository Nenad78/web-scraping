from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import time

condos = [
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/vracar-lokacija-juzni-bulevar-adresa-vojvode-hrvoja-beograd/1958955/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/vozdovac-autokomanda-trise-kaclerovica-90m2-trise-kaclerovica/NkvU3_gZyb6/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/vracar-prote-mateje-78m2-id1187/NkwQVDgJqsw/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/palilula-botanicka-basta-bulevar-despota-stefana-60m2-bulevar-despota-stefana/1734451/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/palilula-postanska-stedionica-dalmatinska-94m2-dalmatinska/Nk1bTYWifZj/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/stari-grad-kalemegdan-strahinjica-bana-37m2-strahinjica-bana/NklcRCutVNB/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/palilula-borca-moravske-divizije-73m2-moravske-divizije/207667/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/palilula-visnjicka-banja-slobodana-jovanovica-75m2-slobodana-jovanovica/Nk2nu-zdbzW/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/zvezdara-mirijevo-jovanke-radakovic-61m2-jovanke-radakovic/NkW5Qg22seE/',
    'https://www.nekretnine.rs/stambeni-objekti/stanovi/zvezdara-deram-pijaca-duke-dinic-80m2-duke-dinic/Nk26as4b71N/']

condo_agency_home_phones = []
condo_agency_cell_phones = []

path_to_extension = r'C:\Users\Nenad\Desktop\3.42.0_0'
options = Options()
options.add_argument('load-extension=' + path_to_extension)
options.headless = False
driver = webdriver.Chrome('/Users/Nenad/chromedriver', options=options)
driver.create_options()
for condo in condos:
    driver.get(condo)
    try:
        wait = WebDriverWait(driver, 10)
        element = wait.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "//button[contains(text(),'broj')]")))
        element.click()
        time.sleep(1)
        home_phone = wait.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "(//span[@class='cell-number'])[1]")))
        condo_agency_home_phones.append(home_phone.text)

        wait1 = WebDriverWait(driver, 10)
        element2 = wait1.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "//button[contains(text(),'broj')]")))
        element2.click()
        time.sleep(1)
        wait2 = WebDriverWait(driver, 10)
        cell_phone = wait2.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "(//span[@class='cell-number'])[2]")))
        condo_agency_cell_phones.append(cell_phone.text)
    except:
        condo_agency_home_phones.append('NaN')
        condo_agency_cell_phones.append('NaN')
driver.close()
print(condo_agency_home_phones)
print(condo_agency_cell_phones)
