from requests import get
from bs4 import BeautifulSoup
from time import sleep, time
from random import randint
from warnings import warn


def scraping_links(links: list, from_page_number: int, to_page_number: int, start_time: float) -> list:
    url = 'https://www.nekretnine.rs/stambeni-objekti/stanovi/izdavanje-prodaja/prodaja/grad/beograd/lista/po_stranici/10/'

    # Priprema za praćenje petlje

    requests = from_page_number
    pages = [str(i) for i in range(from_page_number, to_page_number)]

    # Za prvih 100 stranica

    for page in pages:

        # Pravljenje get zahteva

        # Za prvu stranicu

        if from_page_number == 1:
            response = get(url)

        # Za ostale stranice

        else:
            response = get(url + 'stranica/' + page)

        # Pauziranje petlje

        sleep(randint(20, 40))

        # Praćenje zahteva

        elapsed_time = time() - start_time
        print('Request:{}; Frequency: {} requests/s'.format(requests, requests / elapsed_time))
        requests += 1

        # Upozorenje za status kod različit od 200

        if response.status_code != 200:
            warn('Request: {}; Status code: {}'.format(requests, response.status_code))

        # Prekidanje petlje ako je broj zahteva veći od očekivanog

        if requests > to_page_number:
            warn('Number of requests was greater than expected.')
            break

        # Dodeljivanje stranice u BeautifulSoup

        page_html = BeautifulSoup(response.text, 'html.parser')

        # Selektovanje svih 10 kontejnera na trenutnoj stranici

        page_containers = page_html.find_all('div', class_='row offer')

        # Izvlačenje svih 10 linkova sa trenutne stranice

        for page_container in page_containers:
            # Skrepovanje linkova

            link = 'https://www.nekretnine.rs' + page_container.h2.a['href']
            links.append(link)

    return links


def scraping_data(url: str, category_text: str, category_list: list) -> list:
    response = get(url)
    condo_html = BeautifulSoup(response.text, 'html.parser')

    # Pravljenje liste za odeređenu kategoriju

    try:
        category = condo_html.find('dt', string=category_text + ': ').find_next_siblings('dd')
        category_list.append(category[0].text.strip())
    except:
        category_list.append('NaN')

    return category_list
