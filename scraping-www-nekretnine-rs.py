from time import time, sleep
from random import randint
from moduls.scraping import scraping_links, scraping_data
from requests import get
from bs4 import BeautifulSoup
from warnings import warn
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
import csv

links = []
start_time = time()
condos = scraping_links(links, 1, 2, start_time)
sleep(randint(20, 40))
condos = list(set(scraping_links(condos, 2, 61, start_time)))
condo_price = []
condo_category = []
condo_street = []
condo_city = []
condo_country = []
condo_transaction = []
condo_city_area = []
condo_county = []
condo_square = []
condo_communications = []
condo_heating = []
condo_condition = []
condo_floor = []
condo_register = []
condo_equipment = []
condo_type = []
condo_security = []
condo_total_floors = []
condo_rooms = []
condo_transport = []
condo_year = []
condo_agency_names = []
condo_agency_addresses_unfinished = []
condo_agency_home_phones = []
condo_agency_cell_phones = []
requests = 1
path_to_extension = r'C:\Users\Nenad\Desktop\3.42.0_0'
options = Options()
options.add_argument('load-extension=' + path_to_extension)
options.headless = False
driver = webdriver.Chrome('/Users/Nenad/chromedriver', options=options)
driver.create_options()
for condo in condos:

    # Pravljenje get zahteva

    response = get(condo)
    sleep(randint(20, 40))
    condo_html = BeautifulSoup(response.text, 'html.parser')

    # Praćenje zahteva

    elapsed_time = time() - start_time
    print('Request:{}; Frequency: {} requests/s'.format(requests, requests / elapsed_time))
    requests += 1

    # Upozorenje za status kod različit od 200

    if response.status_code != 200:
        warn('Request: {}; Status code: {}'.format(requests, response.status_code))

    # Prekidanje petlje ako je broj zahteva veći od očekivanog

    # if requests > len(condos):
    #     warn('Number of requests was greater than expected.')
    #     break

    # Izvlačenje cene

    try:
        condo_price.append(condo_html.find('span', class_='mt-auto').text)
    except:
        condo_price.append(None)

    # Izvlačenje ostalih podataka

    condo_category = scraping_data(condo, 'Kategorija', condo_category)
    condo_street = scraping_data(condo, 'Ulica', condo_street)
    condo_city = scraping_data(condo, 'Grad', condo_city)
    condo_country = scraping_data(condo, 'Zemlja', condo_country)
    condo_transaction = scraping_data(condo, 'Transakcija', condo_transaction)
    condo_city_area = scraping_data(condo, 'Deo Grada', condo_city_area)
    condo_county = scraping_data(condo, 'Okrug', condo_county)
    condo_square = scraping_data(condo, 'Kvadratura', condo_square)
    condo_communications = scraping_data(condo, 'Komunikacioni kanali', condo_communications)
    condo_heating = scraping_data(condo, 'Grejanje', condo_heating)
    condo_condition = scraping_data(condo, 'Stanje nekretnine', condo_condition)
    condo_floor = scraping_data(condo, 'Sprat', condo_floor)
    condo_register = scraping_data(condo, 'Uknjiženo', condo_register)
    condo_equipment = scraping_data(condo, 'Opremljenost objekta', condo_equipment)
    condo_type = scraping_data(condo, 'Tip stavke', condo_type)
    condo_security = scraping_data(condo, 'Sigurnost', condo_security)
    condo_total_floors = scraping_data(condo, 'Ukupan brој spratova', condo_total_floors)
    condo_rooms = scraping_data(condo, 'Ukupan broj soba', condo_rooms)
    condo_transport = scraping_data(condo, 'Dostupnost transporta', condo_transport)
    condo_year = scraping_data(condo, 'Godina izgradnje', condo_year)

    # Izvlačenje podataka od agencije

    # Ime agencije

    try:
        condo_agency_names.append(condo_html.h4.text)
    except:
        condo_agency_names.append('NaN')

    # Adresa agencije

    try:
        page = condo_html.find('figcaption', 'd-flex flex-column')
        page_string = str(page)
        full_address = page_string.split('\n')
        addresses = []
        for word in full_address:
            if '<br/>' in word:
                addresses.append(word.strip('<br/>').strip())
        condo_agency_addresses_unfinished.append(addresses)
    except:
        condo_agency_addresses_unfinished.append('NaN')

    # Brojevi telefona agencije

    driver.get(condo)
    try:
        wait = WebDriverWait(driver, 10)
        element = wait.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "//button[contains(text(),'broj')]")))
        element.click()
        sleep(1)
        home_phone = wait.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "(//span[@class='cell-number'])[1]")))
        condo_agency_home_phones.append(home_phone.text)

        wait1 = WebDriverWait(driver, 10)
        element2 = wait1.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "//button[contains(text(),'broj')]")))
        element2.click()
        sleep(1)
        wait2 = WebDriverWait(driver, 10)
        cell_phone = wait2.until(
            expected_conditions.element_to_be_clickable((By.XPATH, "(//span[@class='cell-number'])[2]")))
        condo_agency_cell_phones.append(cell_phone.text)
    except:
        condo_agency_home_phones.append('NaN')
        condo_agency_cell_phones.append('NaN')
driver.close()

# Obrada izvučenih podataka

condo_street_alter = []
for street in condo_street:
    condo_street_alter.append(street.strip(','))
condo_price_alter = []
for price in condo_price:
    condo_price_alter.append(price.strip('EUR').replace(' ', ''))
condo_square_alter = []
for square in condo_square:
    condo_square_alter.append(square.strip('m2').replace(' ', ''))
condo_agency_addresses = []
for address in condo_agency_addresses_unfinished:
    combined_address = ', '.join(address)
    condo_agency_addresses.append(combined_address)

rows = zip(condo_price_alter, condo_category, condo_rooms, condo_street_alter, condo_city, condo_country,
           condo_transaction, condo_city_area, condo_county, condo_square_alter, condo_communications, condo_heating,
           condo_condition, condo_floor, condo_total_floors, condo_register, condo_equipment, condo_type,
           condo_security, condo_transport, condo_year, condo_agency_names, condo_agency_addresses,
           condo_agency_home_phones, condo_agency_cell_phones)

header = ['Price [EUR]', 'Category', 'Rooms', 'Street', 'City', 'Country', 'Transaction', 'City area', 'County',
          'Square [m2]', 'Communications', 'Heating', 'Condition', 'Floor', 'Total floors', 'Register', 'Equipment',
          'Type', 'Security', 'Transport availability', 'Year of building', 'Agency name', 'Agency address',
          'Agency home phone', 'Agency cell phone']

with open('condo_unfinished.csv', "w") as f:
    writer = csv.writer(f)
    writer.writerow(i for i in header)
    for row in rows:
        writer.writerow(row)

# Brisanje duplikata

with open('condo_unfinished.csv') as f:
    data = list(csv.reader(f))
    new_data = [a for i, a in enumerate(data) if a not in data[:i]]
    with open('condo.csv', 'w') as t:
        write = csv.writer(t)
        write.writerows(new_data)

# Brisanje praznih redova

with open('condo.csv') as input, open('condo_final.csv', 'w', newline='') as output:
    writer = csv.writer(output)
    for row in csv.reader(input):
        if any(field.strip() for field in row):
            writer.writerow(row)
